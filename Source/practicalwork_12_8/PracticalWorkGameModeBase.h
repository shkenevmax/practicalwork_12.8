// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PracticalWorkGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PRACTICALWORK_12_8_API APracticalWorkGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
protected:

	virtual void BeginPlay() override;

public:

	
};
