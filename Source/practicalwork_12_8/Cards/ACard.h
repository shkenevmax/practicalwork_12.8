// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "practicalwork_12_8/practicalwork_12_8.h"
#include "ACard.generated.h"

UCLASS()
class PRACTICALWORK_12_8_API ACard : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACard();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Sets all the necessary variables for the playing card 
	*@param SuitIndex - the serial number of the suit playing card in the ESuit enumeration
	*@param RankIndex - the serial number of the rank playing card in the ERank enumeration
	*@param SuitName - the text name of a specific suit
	*@param RankName - the text name of a specific rank
	*@param CardRank - a variable from the ERank enumeration
	*@param CardSuit - a variable from the ESuit enumeration
	*@param CardVal - dignity of the card in accordance with the rules of the game Black jack
	*/
	void SetCardValues(int SuitIndex, 
					   int RankIndex, 
					   FString SuitName,
					   FString RankName, 
					   ERank CardRank, 
					   ESuit CardSuit, 
				   	   int CardVal);

	/** The structure of unique variables belonging to a unique playing card */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FUniqueCardInfo UniqueCardInfo;
};
