// Fill out your copyright notice in the Description page of Project Settings.


#include "practicalwork_12_8/Cards/ACard.h"

// Sets default values
ACard::ACard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACard::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACard::SetCardValues(int SuitIndex, 
						  int RankIndex, 
						  FString SuitName, 
						  FString RankName, 
						  ERank CardRank, 
						  ESuit CardSuit, 
						  int CardVal)
{
	// Set a unique index for the playing card, based on the suit and rank indexes
	UniqueCardInfo.CardIndex = SuitIndex + (RankIndex * 14);

	// Set the name of the playing card by combining the name suit and the name rank
	UniqueCardInfo.CardName = SuitName + RankName;

	// The rest of the parameters are simply passed to the structure
	UniqueCardInfo.CardRank = CardRank;
	UniqueCardInfo.CardSuit = CardSuit;
	UniqueCardInfo.CardValue = CardVal;
}

