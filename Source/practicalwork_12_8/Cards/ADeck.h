// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "practicalwork_12_8/Cards/ACard.h"
#include "practicalwork_12_8/PracticalWorkGameModeBase.h"
#include "ADeck.generated.h"

UCLASS()
class PRACTICALWORK_12_8_API ADeck : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADeck();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Generates a deck of playing cards */
	void InitDeckOfCards();

	/** Returns an array of pointers to the generated deck of cards */
	UFUNCTION(BlueprintCallable)
	TArray<ACard*> GetDeckOfCards();

	/** An array of pointers to the generated deck of cards */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ACard*> Cards;

	/** Shuffles the deck of cards */
	UFUNCTION(BlueprintCallable)
	void ShuffleCards();
};
