// Fill out your copyright notice in the Description page of Project Settings.


#include "practicalwork_12_8/Cards/ADeck.h"
#include "practicalwork_12_8/PracticalWorkGameInstance.h"

// Sets default values
ADeck::ADeck()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADeck::BeginPlay()
{
	Super::BeginPlay();
	
	InitDeckOfCards();
}

// Called every frame
void ADeck::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADeck::InitDeckOfCards()
{
	ESuit CardSuit = ESuit::NONE;
	FString SuitName;

	// Iterating through the enumeration of suit
	for (int i = 1; i < 5; i++)
	{
		CardSuit = ESuit(i);

		switch (ESuit(i))
		{
		case ESuit::NONE:
			SuitName = TEXT("NONE ");
			break;
		case ESuit::Diamonds:
			SuitName = TEXT("Diamonds ");
			break;
		case ESuit::Spades:
			SuitName = TEXT("Spades ");
			break;
		case ESuit::Clubs:
			SuitName = TEXT("Clubs ");
			break;
		case ESuit::Hearts:
			SuitName = TEXT("Hearts ");
			break;
		default:
			break;
		}

		// Iterate through the enumeration of ranks, create a playing card object and set unique characteristics for it
		for (int j = 1; j < 14; j++)
		{
			ACard* NewCard = NewObject<ACard>();;
			switch (ERank(j))
			{
			case ERank::NONE:
				break;
			case ERank::Ace:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Ace")), ERank(j), ESuit(i), 11);
				break;
			case ERank::King:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("King")), ERank(j), ESuit(i), 10);
				break;
			case ERank::Queen:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Queen")), ERank(j), ESuit(i), 10);
				break;
			case ERank::Jack:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Jack")), ERank(j), ESuit(i), 10);
				break;
			case ERank::Ten:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Ten")), ERank(j), ESuit(i), 10);
				break;
			case ERank::Nine:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Nine")), ERank(j), ESuit(i), 9);
				break;
			case ERank::Eight:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Eight")), ERank(j), ESuit(i), 8);
				break;
			case ERank::Seven:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Seven")), ERank(j), ESuit(i), 7);
				break;
			case ERank::Six:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Six")), ERank(j), ESuit(i), 6);
				break;
			case ERank::Five:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Five")), ERank(j), ESuit(i), 5);
				break;
			case ERank::Four:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Four")), ERank(j), ESuit(i), 4);
				break;
			case ERank::Three:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Three")), ERank(j), ESuit(i), 3);
				break;
			case ERank::Two:
				NewCard->SetCardValues(j, i, SuitName, FString(TEXT("Two")), ERank(j), ESuit(i), 2);
				break;
			default:
				break;
			}

			// Adding a pointer to the created playing card to the array of pointers
			Cards.Add(NewCard);
		}
	}

	// Passing an array of playing card pointers (a deck of playing cards) to the PracticalWorkGameInstance variable
	UPracticalWorkGameInstance* PracticalWorkGameInstance = Cast<UPracticalWorkGameInstance>(GetGameInstance());
	if (PracticalWorkGameInstance)
	{
		PracticalWorkGameInstance->DeckOfCards = Cards;
	}
}

TArray<ACard*> ADeck::GetDeckOfCards()
{
	return Cards;
}

void ADeck::ShuffleCards()
{
	// Iterate 50 times over the entire deck of playing cards
	for (int j = 0; j < 50; j++)
	{
		// Iterate over all the cards of the deck of playing cards
		for (int i = 1; i < Cards.Num(); i++)
		{
			// Randomly select the cards to change places
			if (FMath::RandBool())
			{
				// Write the index of the current card to the local variable
				int OldCardIndex = Cards[i]->UniqueCardInfo.CardIndex;

				//Assign the index of the previous playing card to the current playing card
				Cards[i]->UniqueCardInfo.CardIndex = Cards[i - 1]->UniqueCardInfo.CardIndex;

				// Assign the index of the current playing card to the previous playing card, previously recorded in a local variable
				Cards[i - 1]->UniqueCardInfo.CardIndex = OldCardIndex;
			}
		}
	}
	
	// Sort the deck of playing cards based on the indexes of the playing cards
	Cards.Sort([](const ACard& A, const ACard& B) {
		return A.UniqueCardInfo.CardIndex > B.UniqueCardInfo.CardIndex;
		});

	// Passing an array of playing card pointers (a deck of playing cards) to the PracticalWorkGameInstance variable
	UPracticalWorkGameInstance* PracticalWorkGameInstance = Cast<UPracticalWorkGameInstance>(GetGameInstance());
	if (PracticalWorkGameInstance)
	{
		PracticalWorkGameInstance->DeckOfCards = Cards;
	}
}

