// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "practicalwork_12_8/Cards/ACard.h"
#include "PracticalWorkGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PRACTICALWORK_12_8_API UPracticalWorkGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ACard*> DeckOfCards;
	
};
