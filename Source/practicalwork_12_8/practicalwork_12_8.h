// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "practicalwork_12_8.generated.h"

/** An enumeration containing the suit of the cards of a standard French playing deck */
UENUM(BlueprintType)
enum class ESuit : uint8
{
	NONE			UMETA(DisplayName = "Default"),
	Diamonds		UMETA(DisplayName = "Diamonds"),
	Spades			UMETA(DisplayName = "Spades"),
	Clubs			UMETA(DisplayName = "Clubs"),
	Hearts			UMETA(DisplayName = "Hearts")
};

/** An enumeration containing the rank of the cards of a standard French playing deck */
UENUM(BlueprintType)
enum class ERank : uint8
{
	NONE			UMETA(DisplayName = "Default"),
	Ace				UMETA(DisplayName = "Ace"),
	King			UMETA(DisplayName = "King"),
	Queen			UMETA(DisplayName = "Queen"),
	Jack			UMETA(DisplayName = "Jack"),
	Ten				UMETA(DisplayName = "Ten"),
	Nine			UMETA(DisplayName = "Nine"),
	Eight			UMETA(DisplayName = "Eight"),
	Seven			UMETA(DisplayName = "Seven"),
	Six				UMETA(DisplayName = "Six"),
	Five			UMETA(DisplayName = "Five"),
	Four			UMETA(DisplayName = "Four"),
	Three			UMETA(DisplayName = "Three"),
	Two				UMETA(DisplayName = "Two")
};

USTRUCT(BlueprintType)
struct FUniqueCardInfo
{
	GENERATED_BODY()

	/** Sequential number of the card assigned when creating a deck of cards and changed when shuffling */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CardIndex = 0;

	/** Dignity of the card in accordance with the rules of the game Black jack */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CardValue = 0;

	/** Name of the card, consisting of suit and rank */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString CardName = TEXT("None");

	/** Card suit variable */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ESuit CardSuit = ESuit::NONE;

	/** Card rank variable */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ERank CardRank = ERank::NONE;
};
